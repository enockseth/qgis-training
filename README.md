# Free and Open Source Software for Geographic Information Systems (FOSSGIS) and Geo Data Visualization Training using QGIS

Introduction to Open Source Geographic Information Systems (GIS) and Data Visualisation training using QGIS and Open Data f.e. OpenStreetMap, Natural Earth, etc

## Requirements
- Download and Install [QGIS](https://qgis.org/) version 3.x.x and above
- A mouse (_highly recommended_)
- Download [Slides](./introduction_to_gis_using_qgis.pdf)
- Download [Data](./qgis_training_data.gpkg)

## Overview
### Introduction
- Brief Description of Open Source GIS and an Introduction to QGIS 
- Preparing data for use in the training. ­ This is mainly to show people how data can be acquired for their local area of interest

### The Interface
- Understanding the whole QGIS interface and a description of the tools
- Adding Creating and editing vector layers
- Interaction between vector layer and QGIS interface

### Creating a Basic Map
- Creating / Finding vector data
- Adding layers in QGIS and understanding vector Symbology
- Adding your vector layers from various sources into QGIS
- Understanding vector symbology
- Different types of styling. Single symbols versus Categorised and Graduated symbols.
- Other remaining types of symbology

### Classifying Vector Data ­
- Understanding attribute data.
- Labelling data
- Vector classification

### Extras
- Data Sources
- Plugins
- Community & Support
